package example.checkedExceptions;

public class Checked {
    public static void main(String[] args) {
      //   Checked Exceptions: Hiervoor gelden alle regels zoals try, catch of throws. Het zijn voorstelbare uitzonderingen
        // die op zouden kunnen treden.
        //Bij een Checked Exception moet je OF:
        //- Het met een try en catch block afvangen.
        //- Of het via throws in de methode door moeten geven naar de aanroepende methode
        try {
            pensioenfondsZoeken("KPN"); //pas dit aan naar andere fondscode en kijk wat er gebeurt!!!!
            System.out.println("hier komen we alleen indien geen exception");
        } catch (PensioenfondsNotFoundException p) {
            System.err.println(p.getMessage());
        }

        // onderstaande mag niet want de trowsede exception vanuit method wordt niet gecatched. Uncomment maar!!!
        // pensioenfondsZoeken("");
    }

    static Pensioenfonds pensioenfondsZoeken(String fondscode) throws PensioenfondsNotFoundException {
        if(fondscode.equals("KPN")){
            return new Pensioenfonds(fondscode);
        }

        throw new PensioenfondsNotFoundException();
    }
}
