package example.checkedExceptions;

public class PensioenfondsNotFoundException extends Exception {
    PensioenfondsNotFoundException(){
        super("pensioenfonds niet gevonden");
    }
}
