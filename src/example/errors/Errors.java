package example.errors;

public class Errors {
    public static void main(String[] args) {

        //Zonder try/catch compileert het wel EN je hoeft niet te throwsen. Uncomment hieronder.
        //overklokken();

        try {
            overklokken();
        } catch (Exception e) {
            System.out.println("Dit wordt niet uitgevoerd");
        } catch (Throwable t) {

//            Bij meerdere catch-blokken moet je altijd van Klein naar groot opvolgen.
            System.out.println("Dit wordt wel uitgevoerd want Error is covariant");
        }
    }

    static void overklokken() {
        throw new RamGesmoltenError();
    }
}