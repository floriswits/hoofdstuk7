package example.finallyexample;

//Een try block heeft altijd OF een catchblok nodig OF een finally block OF allebei.
//Het finally block wordt altijd uitgevoerd. (behalve bij System.exit)
class Finally {
    public static void main(String[] args) {

        try {
            throw new Exception("exceptie!!!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
        } finally {
            System.out.println("finally na try");
        }

        try {
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("finally na catch");
        }


        //uitzondering: een finally die niet altijd wordt uitgevoerd
//        try {
//            System.exit(1);
//        } finally {
//            System.out.println("finally wordt nooit bereikt");
//        }

        //Als een Exception niet gecatched wordt/afgehandeld wordt dan stopt de executie van het programma meteen.
        System.out.println("wordt wel uitgevoerd");
        int i = 5;
        if (i == 5) {
            //Een Exceptie die niet gecatched wordt geeft een StackTrace als output. Dan toont hij welke aanroepende
            //methodes door de fout vastlopen.
            throw new FatalityException();
        }
        //onderstaande komen we dus nooit
        System.out.println("wordt niet uitgevoerd");
    }
}