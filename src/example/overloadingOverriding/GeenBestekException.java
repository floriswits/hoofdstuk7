package example.overloadingOverriding;

public class GeenBestekException extends Exception {
    public GeenBestekException() {
        super("ork ork ork, soep eet je zonder vork");
    }
    public GeenBestekException(String bericht) {
        super(bericht);
    }
}
