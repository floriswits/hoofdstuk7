package example.overloadingOverriding;

public class Kind extends Ouder {
    //deze constructor maken we, omdat ie anders niet compiled (moet voldoen aan constructor ouder)(zie kind2)
    public Kind() throws GeenBestekException {
    }

    //Bij overloading is sprake van compleet verschillende functies dus hier heeft het geen invloed.
    void eten() throws Exception {
        throw new Exception();
    }

    // onderstaande mogen we niet doen ivm overridingproblemen doordat exception geen covariant is. (Exception>GeenBestekException)
//    void eten(Bestek bestek) throws Exception {
//        throw new Exception();
//    }

    // onderstaande mogen we wel doen want het is een covariant (GeenlepelException<GeenBestekException)
    void eten(Bestek bestek) throws GeenLepelException {
        throw new GeenLepelException();
    }

    // onderstaande mag wel want is geen exception -hierboven wel ff uitcommenten.
//    void eten(Bestek bestek)  {
//
//    }
}
