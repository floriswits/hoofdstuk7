package example.overloadingOverriding;

public class Kind2 extends Ouder {
    //Bij overriding van een Constructor mag het kind alleen een grotere of gelijke throwsen dan zijn ouder.
    //onderstaande mag niet, want exception is kleiner dan ouder
//    Kind2(Bestek bestek) throws GeenLepelException {
//        super();
//        throw new GeenLepelException();
//    }

    //Deze throwsed een even grote exception als ouder
    Kind2(Bestek bestek, int a) throws GeenBestekException {
        super();
        throw new GeenBestekException();
    }

    //Deze throwsed een grotere exception dan ouder
    Kind2() throws Exception {
        super();
        throw new Exception();
    }
}
