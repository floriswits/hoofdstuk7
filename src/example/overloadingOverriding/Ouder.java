package example.overloadingOverriding;

public class Ouder {
    public Ouder() throws GeenBestekException{
        throw new GeenBestekException();
    }

    void eten(Bestek bestek) throws GeenBestekException {
        if(bestek == null){
            throw new GeenBestekException();
        }
        System.out.println("met mes en vork");
    }
}
