package example.rethrow;
//Als zowel het try-block een exception throwed, en het catch block deze rethrowed en de finally een andere
//        exception throwed. Wordt de andere exception gethrowed daar een methode maar 1 exception kan uitgooien.
//        (Ook zal finally eventuele variabelen overschrijven)
public class rethrow {
    public static void main(String[] args) throws KleineException {
        try {
            gooien();
        } catch (GroteException g) {
            throw g;
        } finally {
            throw new KleineException();
        }
    }
    static void gooien() throws GroteException{
        throw new GroteException();
    }
}
