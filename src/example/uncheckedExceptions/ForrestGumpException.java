package example.uncheckedExceptions;

public class ForrestGumpException extends RuntimeException {
    ForrestGumpException() {
        super("Run, Forrest, Run!");
    }
}
