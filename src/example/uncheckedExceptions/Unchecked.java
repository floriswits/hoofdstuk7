package example.uncheckedExceptions;

public class Unchecked {

    public static void main(String[] args) {

        //uncomment lopen. Dan compileert het ook! Want runtimeException en unchecked.
        //lopen();

        //onderstaande werkt want exceptie wordt netjes gevangen. Maar is niet nodig! Kijk maar hierboven.
        try{
            lopen();
        } catch (ForrestGumpException f) {
            System.out.println(f.getMessage());
        }
    }

    static void lopen(){
        throw new ForrestGumpException();
    }
}